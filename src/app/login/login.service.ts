import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })
export class LoginService {
    url = 'https://run.mocky.io/v3/9ba22c8c-0b4e-402a-a9fa-36dda35982f7';

    constructor(private http: HttpClient) {}

    login(username: string, password: string): Observable<any> {
        const userData = {
            username: username,
            password: btoa(password)
        };
        return this.http.post(this.url, userData);
    }
}