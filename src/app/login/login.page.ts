import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {

  username = '';
  password = '';

  constructor(private loginService: LoginService,
    public navCtrl: NavController,
    private menu: MenuController) { }

  ngOnInit() {
    this.menu.enable(false, 'main-menu');
  }

  login() {
    this.loginService.login(this.username, this.password).subscribe(data => {
      if(data.userEmail != null) {
        sessionStorage.setItem('userEmail', atob(data.userEmail));
        this.navCtrl.navigateRoot('/orderlist');
      }
    }, error => {
      console.log(error);
    });
  }

}
