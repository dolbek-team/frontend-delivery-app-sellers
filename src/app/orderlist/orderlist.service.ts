import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({ 
      'Access-Control-Allow-Origin':'*',
      'userid':'1',
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
  })
export class OrderListService {
    url = 'http://35.193.212.67:8080/api/purchase-order-header';
    urlTracking = 'http://35.193.212.67:8080/tracking';
    //url = 'https://run.mocky.io/v3/b3c20652-18f5-4009-a120-0238bc714f64';

    constructor(private http: HttpClient) {}

    getOrders(): Observable<any> {
        return this.http.get(this.url, httpOptions);
    }

    saveStatus(orderId: string, status: number): Observable<any> {
      const body = {
          order: orderId,
          status: status
      };
      return this.http.post(this.urlTracking,body, httpOptions);
  }
}