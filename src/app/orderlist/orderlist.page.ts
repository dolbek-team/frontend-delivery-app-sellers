import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { OrderListService } from './orderlist.service'
import { Orderlist } from './orderlist.model';
import { DateModel } from './date.model';
import { DatePipe } from '@angular/common';

import { addDays, isEqual, format } from 'date-fns';

@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.page.html',
  styleUrls: ['./orderlist.page.scss'],
  providers: [DatePipe]
})
export class OrderlistPage implements OnInit {

  constructor(private orderListService: OrderListService, public datepipe: DatePipe, public navCtrl: NavController) { }

  orderList: Array<Orderlist>;
  dateList: Array<string> = new Array;

  ngOnInit() {
    this.getOrders();
  }

  ionViewDidEnter() {
    this.dateList = new Array;
    this.getOrders();
  }

  getOrders() {
    this.orderListService.getOrders().subscribe(data => {
      // Remove orders delivered
      this.orderList = this.cleanOrders(data);

      // Get dates
      this.orderList.forEach(order => {
        if(!this.dateList.includes(order.deliveryDate)) {
          this.dateList.push(order.deliveryDate);
        }
      });

      this.sortDates();
    }, error => {
      console.log(error);
    })
  }

  getListByDate(date: string) {
    return this.orderList.filter(order => order.deliveryDate == date);
  }

  getLabelFecha(date: string) {
    const todayDate = new Date();
    const today = format(todayDate, 'yyyy-MM-dd');
    const tomorrow = format(todayDate.setDate(todayDate.getDate() + 1), 'yyyy-MM-dd');
    if(today === date) return 'Hoy';
    if(tomorrow === date) return 'Mañana';
    else  return this.formatFecha(date);
  }

  formatFecha(date: string) {
    const strings = date.split('-');
    return strings[2] + "-" + strings[1] + "-" + strings[0];
  }

  getStatusDescription(id: number) {
    let desc = '';
    switch (id) {
      case 1:
        desc = 'EMPQ';
        break;
      case 2:
        desc = 'RUTA';
        break;
      case 3:
        desc = 'FIN';
        break;
      case 4:
        desc = 'PEND'
        break;
      default:
        break;
    }

    return desc;
  }

  goToDetail(orderId: number) {
    this.navCtrl.navigateForward('/orderlist/detail/' + orderId);
  }

  sortDates() {
    this.dateList = this.dateList.sort((n1,n2) => {
      if (n1 > n2) {
          return 1;
      }
  
      if (n1 < n2) {
          return -1;
      }
  
      return 0;
  });
  }

  cleanOrders(data: Array<Orderlist>) {
    return data.filter(order => order.status !== 3);
  }
}
