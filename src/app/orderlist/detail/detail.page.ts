import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  constructor(private _Activatedroute:ActivatedRoute, private navCtrl: NavController) { }
  orderId: string;

  sub;
 
   ngOnInit() {
      this.sub=this._Activatedroute.paramMap.subscribe(params => { 
        console.log(params);
        this.orderId = params.get('id');     
      });
   }
 
   ngOnDestroy() {
     this.sub.unsubscribe();
   }

   goToProductList() {
    this.navCtrl.navigateForward('/orderlist/products/' + this.orderId);
   }
}
