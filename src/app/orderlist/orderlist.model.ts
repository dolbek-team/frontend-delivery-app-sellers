export class Orderlist {
    deliveryDate: string;
    orderHeaderId: number;
    status: number;
    timeRange: string;
};