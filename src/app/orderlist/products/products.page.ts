import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ProductsService } from './products.service'
import { OrderListService } from '../orderlist.service';
import { Products } from './products.model'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

  classFooter: string = 'footer-color-disabled';
  classButton: boolean = false;

  constructor(private _Activatedroute:ActivatedRoute, 
    public navCtrl: NavController, 
    private productService: ProductsService,
    private orderlistService: OrderListService) { }
  orderId: string;
  productList: Array<Products>;
  firstMark: boolean;
  packagedProducts: Array<number> = new Array;

  sub;
 
   ngOnInit() {
      this.sub=this._Activatedroute.paramMap.subscribe(params => { 
        this.orderId = params.get('id'); 
        this.getProducts();    
      });
      this.firstMark = false;
   }
 
   ngOnDestroy() {
     this.sub.unsubscribe();
   }

   getProducts() {
      this.productService.getProducts(this.orderId).subscribe(data => {
        this.productList = data;
      }, error => {
        console.log(error);
      })
   }

  stateChange(event: any, prod: Products) {
    // Save packaged item
    if(event) {
      this.packagedProducts.push(prod.orderDetailId);
    } else {
      this.packagedProducts.splice(this.packagedProducts.indexOf(prod.orderDetailId));
    }

    // Check order status
    if(!this.firstMark) {
      // Send a change of status
      console.log('status change!');
      this.firstMark = true;
      this.orderlistService.saveStatus(this.orderId, 1).subscribe(result => {
      }, error => {
        console.log(error);
      });
    }

    // Verify if all toggle items are checked
    if(this.packagedProducts.length === this.productList.length) {
      this.classFooter = 'footer-color-enabled';
      this.classButton = true;
    } else {
      this.classFooter = 'footer-color-disabled';
    }
  }

  goToTransport() {
    if(this.classButton) {
      this.orderlistService.saveStatus(this.orderId, 2).subscribe(result => {
        this.navCtrl.navigateForward('/orderlist/transport/' + this.orderId);
      }, error => {
        console.log(error);
      });
    }
  }
}
