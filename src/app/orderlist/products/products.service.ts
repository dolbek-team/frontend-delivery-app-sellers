import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })
export class ProductsService {
    //url = 'https://run.mocky.io/v3/58708949-c3ad-4d30-86df-6b0fc5d830ea';
    url = 'http://35.193.212.67:8080/api/purchase-order-detail/';

    constructor(private http: HttpClient) {}

    getProducts(orderId: string): Observable<any> {
        return this.http.get(this.url + "/" + orderId);
    }
}