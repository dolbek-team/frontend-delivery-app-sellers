export class Products {
    orderDetailId: number;
    orderHeaderId: number;
    sku: string;
    skuDescription: string;
    packing: boolean;
}