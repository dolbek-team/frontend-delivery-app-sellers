import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { OrderListService } from '../orderlist.service';

@Component({
  selector: 'app-transport',
  templateUrl: './transport.page.html',
  styleUrls: ['./transport.page.scss'],
})
export class TransportPage implements OnInit {

  orderId: string;

  constructor(private _Activatedroute:ActivatedRoute,
    public alertController: AlertController,
    public navCtrl: NavController,
    private orderlistService: OrderListService) { }

  sub;
 
   ngOnInit() {
      this.sub=this._Activatedroute.paramMap.subscribe(params => { 
        this.orderId = params.get('id');  
      });
   }

   sendStatus() {
     this.presentAlert();
   }

   async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '¡Haz entregado tu producto!',
      message: 'Pronto tendrás el dinero en tu cuenta',
      buttons: [{
        text: 'OK',
        handler: () => {
          this.goToStart();
        }
      }]
    });

    await alert.present();
  }

  saveStatusChange() {
    // Send the status change
    this.orderlistService.saveStatus(this.orderId, 3).subscribe(result => {
      this.presentAlert();
    }, error => {
      console.log(error);
    });
  }

  goToStart(){
    this.navCtrl.navigateRoot('/orderlist');
  }

}
