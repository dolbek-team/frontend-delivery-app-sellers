## FTrack frontend

### App to manage tracking information for sellers who deliver their own products.

#### How to install and test locally
1. Install npm
2. Install ionic cli
3. Run the installation with this command: npm install
4. Run the app with the command: ionic serve --lab

#### Technical information
- Hybrid app
- Made with Ionic Framework, powered by Angular